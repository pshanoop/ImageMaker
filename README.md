ImageMaker
==========

Micro-service to generate scale images for websites from AWS S3.

TODO
-----
- [ ] Add local file system support.
- [ ] Add upload image.
- [ ] Add image processor and optimizer using `spatie/image` and `spatie/image-optimizer`.
- [ ] Add docker image with all requirement `image-optimizer`.