<?php


namespace App\SystemBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


class BaseController extends Controller
{

    /**
     * @Route("/v5/{image}", name="default")
     * @param string $image
     * @return Response
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function indexAction(string $image)
    {
        $flysystem = $this->get('oneup_flysystem.default_filesystem');

        if ($flysystem->has($image)) {
            $fileContent = $flysystem->read($image);
        } else {

            $imageRequest = $this->get('image.request.processor')
                ->getImageRequest();
            $imageGenerator = $this->get('image.generator');
            $fileContent = $imageGenerator->generate($imageRequest, $image);

            if (!$fileContent) {

                return new Response("$image is not found.", Response::HTTP_NOT_FOUND);
            }
        }

        $mimeType = $flysystem->getMimetype($image);
        $fileSize = $flysystem->getSize($image);

        return $this->createFileResponse($image, $fileContent, $mimeType, $fileSize);

    }

    /**
     * @param string $filename
     * @param $fileContent
     * @param string $mimeType
     * @param int $fileSize
     * @return Response
     */
    protected function createFileResponse(string $filename,
                                          $fileContent,
                                          string $mimeType = null,
                                          int $fileSize = null)
    {
        $response = new Response($fileContent);

        $disposition = $response
            ->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_INLINE,
                $filename
            );

        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', $mimeType ?? 'application/octet-stream');
        $response->headers->set('Content-Transfer-Encoding', 'binary');

        if ($fileSize) {
            $response
                ->headers->set('Content-Length', $fileSize);
        }

        return $response;
    }
}