<?php


namespace App\SystemBundle\Service\Image\Filters;


interface ImageFilterInterface
{
    public function getName() : string ;

    public function getWidth();
    public function getHeight();

    public function setWidth(int $widthInPixel);
    public function setHeight(int $widthInPixel);

    public function setBackground(string $hexColor);
    public function getBackground(): string;
}