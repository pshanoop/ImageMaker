<?php


namespace App\SystemBundle\Service\Image\Filters;


class AbstractImageFilter implements ImageFilterInterface
{
    protected $width;
    protected $height;
    protected $hexColor = false;
    protected $name;

    public function __construct() { }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param mixed $widthInPixel
     * @return ImageFilterInterface
     */
    public function setWidth(int $widthInPixel)
    {
        $this->width = $widthInPixel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $widthInPixel
     * @return ImageFilterInterface
     */
    public function setHeight(int $widthInPixel)
    {
        $this->height = $widthInPixel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    public function setBackground(string $hexColor)
    {
        if (preg_match('/#([\da-fA-F]{2})([\da-fA-F]{2})([\da-fA-F]{2})/', $hexColor)) {
            $this->hexColor = $hexColor;
        }
        return $this;
    }

    public function getBackground(): string
    {
        return $this->hexColor;
    }

}