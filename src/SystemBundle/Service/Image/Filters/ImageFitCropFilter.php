<?php


namespace App\SystemBundle\Service\Image\Filters;


class ImageFitCropFilter extends AbstractImageFilter implements ImageFilterInterface
{
    const NAME = 'fit';
    protected $name=self::NAME;
}