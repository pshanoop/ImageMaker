<?php


namespace App\SystemBundle\Service\Image\Filters;


class ImageCropFilter extends AbstractImageFilter implements ImageFilterInterface
{
    const NAME = 'crop';
    protected $name = self::NAME;
}