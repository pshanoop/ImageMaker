<?php


namespace App\SystemBundle\Service\Image;

use App\SystemBundle\Service\Image\Drivers\ImageInterface;
use App\SystemBundle\Service\Image\Filters\ImageCropFilter;
use App\SystemBundle\Service\Image\Filters\ImageFilterInterface;
use App\SystemBundle\Service\Image\Filters\ImageFitCropFilter;
use App\SystemBundle\Service\ImageRequestResolver\DTO\ImageRequest;
use Exception;
use League\Flysystem\FilesystemInterface;
use SplFileInfo;
use Symfony\Component\Routing\RequestContextAwareInterface;

class ImageGenerator
{

    /**
     * @var array
     */
    private $config;


    /**
     * @var RequestContextAwareInterface
     */
    private $requestContext;
    /**
     * @var FilesystemInterface
     */
    private $filesystem;
    /**
     * @var FilesystemInterface
     */
    private $localFs;
    /**
     * @var ImageInterface
     */
    private $imageProcessor;
    /**
     * @var string
     */
    private $cacheDir;

    /**
     * ImageGenerator constructor.
     *
     * @param array $config
     * @param string $cacheDir
     * @param RequestContextAwareInterface $requestContext
     * @param FilesystemInterface $filesystem
     * @param FilesystemInterface $localFs
     * @param ImageInterface $imageProcessor
     */
    public function __construct(
        array $config,
        string $cacheDir,
        RequestContextAwareInterface $requestContext,
        FilesystemInterface $filesystem,
        FilesystemInterface $localFs,
        ImageInterface $imageProcessor
    )
    {
        $this->config = $config;
        $this->requestContext = $requestContext;
        $this->filesystem = $filesystem;
        $this->localFs = $localFs;
        $this->imageProcessor = $imageProcessor;
        $this->cacheDir = $cacheDir;
    }


    /**
     * @param ImageRequest|null $imageRequest
     * @param string $imageName
     * @return false|string
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function generate(ImageRequest $imageRequest = null, string $imageName = null)
    {
        if (!is_null($imageRequest)) {

            $originalFile = $this->getOriginalFileName($imageRequest);
            if($originalFile){

                //Store locally Kraken and many other services's
                //SDK doesn't support sending file contents.
                $fileInfo = $this->storeLocally(
                    $originalFile,
                    $this->filesystem->read($originalFile)
                );

                if($fileInfo){
                    $generatedImage = $this->processRequest($imageRequest,$fileInfo);
                    if ($generatedImage && $this->filesystem->put($imageName, $generatedImage)) {
                        return $generatedImage;
                    }
                }
            }
        }

        return false;
    }

    private function processRequest(ImageRequest $imageRequest, SplFileInfo $file)
    {
        try {
            $this->imageProcessor
                ->openFile($file);

            $this->imageProcessor->applyFilter($this->getFilter($imageRequest));

            if ($imageRequest->getQuality()) {
                $this->imageProcessor->setQuality($imageRequest->getQuality());
            }

        } catch (Exception $e) {
            return false;
        }

        return $this->imageProcessor->generateImage();
    }

    private function getFilter(ImageRequest $imageRequest) : ImageFilterInterface
    {
        return (new ImageFitCropFilter())
            ->setWidth($imageRequest->getWidth())
            ->setHeight($imageRequest->getHeight())
            ->setBackground($imageRequest->getBackground());
    }

    private function storeLocally(string $imageName, string $fileContent)
    {
        if ($this->localFs->put($imageName, $fileContent)) {
            return $this->getFile($imageName);
        }
        return false;
    }


    private function getFile($imageName)
    {
        return new SplFileInfo(
            sprintf("%s/%s", $this->config['directory'], $imageName)
        );
    }

    /**
     * @param ImageRequest $imageRequest
     * @return bool|string
     */
    public function getOriginalFileName(ImageRequest $imageRequest)
    {
        if ($this->filesystem->has($imageRequest->getName())) {

            return $imageRequest->getName();
        } elseif ($this->filesystem
            ->has("{$imageRequest->getName()}.{$imageRequest->getExtension()}")) {

            return $imageRequest->getName().".".$imageRequest->getExtension();
        }

        return false;
    }

}
