<?php


namespace App\SystemBundle\Service\Image\Factory;


use App\SystemBundle\Service\Image\Drivers\KrakenIo;
use App\SystemBundle\Service\Image\Drivers\ImageInterface;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

class ImageProcessorFactory
{
    const KRAKEN = 'kraken';


    private static $available_drivers = [
        self::KRAKEN
    ];
    const DRIVER = 'driver';
    /**
     * @var array
     */


    /**
     * Create object of driver
     * @param array $config
     * @return ImageInterface
     */
    public static function create(array  $config) : ImageInterface
    {
        if(!in_array($config[self::DRIVER],self::$available_drivers)){
            throw new InvalidArgumentException(sprintf('Driver: %s is not available', $config[self::DRIVER] ));
        }

        if($config[self::DRIVER] == self::KRAKEN){
            return new KrakenIo($config, (new \Kraken($config['kraken_key'], $config['kraken_secret'])));
        }
    }

    /**
     * @return array
     */
    public static function getAvailableDrivers(): array
    {
        return self::$available_drivers;
    }
}