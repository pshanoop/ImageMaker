<?php


namespace App\SystemBundle\Service\Image\Drivers;


use App\SystemBundle\Service\Image\Filters\ImageFilterInterface;
use SplFileInfo;

interface ImageInterface
{

    public function openFile(SplFileInfo $file);

    public function openFromUrl(string $url);

    public function setQuality(int $quality);
    public function applyFilter(ImageFilterInterface $imageFilter);

    public function generateImage();
}