<?php


namespace App\SystemBundle\Service\Image\Drivers;


use App\SystemBundle\Service\Image\Filters\ImageCropFilter;
use App\SystemBundle\Service\Image\Filters\ImageFilterInterface;
use App\SystemBundle\Service\Image\Filters\ImageFitCropFilter;
use InvalidArgumentException;
use Kraken;
use SplFileInfo;

/**
 * Class KrakenIo
 * @package App\SystemBundle\Service\Image\Drivers
 *
 * @link https://kraken.io/docs/image-resizing
 */

class KrakenIo implements ImageInterface
{


    private $isFileMode =true;
    const PARAM_RESIZE = 'resize';
    const PARAM_STRATEGY = 'strategy';
    /** @var Kraken  */
    private $kraken;

    /**@var array*/
    private $param=[
        'wait' => true,
        'webp' => false,
        'lossy'=> false,
    ];

    /** @var array  */

    static $available_filter = [
        ImageCropFilter::NAME,
        ImageFitCropFilter::NAME,
    ];

    /**
     * KrakenIo constructor.
     * @param array $config
     * @param Kraken $kraken
     */
    public function __construct(array $config, Kraken $kraken)
    {
         $this->kraken = $kraken;

         $this->param['preserve_meta'] = [
             'profile'
         ];
    }

    public function openFromUrl(string $url)
    {
        $this->setFileMode(false);
        $this->param['url'] = $url;
    }

    public function openFile(SplFileInfo $file)
    {
        $this->setFileMode(true);
        $this->param['file'] = $file->getRealPath();
    }

    public function applyFilter(ImageFilterInterface $imageFilter)
    {
        if(!in_array($imageFilter->getName(),self::$available_filter))
        {
            throw  new \InvalidArgumentException(sprintf("Filter: %s is not allowed in %s", $imageFilter->getName(), get_class($this)));
        }

        $this->param[self::PARAM_RESIZE]['width'] = $imageFilter->getWidth();
        $this->param[self::PARAM_RESIZE]['height'] = $imageFilter->getHeight();

        if ($imageFilter->getName() == ImageCropFilter::NAME) {
            $this->imageCrop($imageFilter);
        } elseif ($imageFilter->getName() == ImageFitCropFilter::NAME) {
            $this->imageFit($imageFilter);
        }

        return $this;
    }

    /**
     * @return bool|string
     */
    public function generateImage()
    {
        if($this->isFileMode) {
            $response = $this->kraken->upload($this->param);
        } else {
            $response = $this->kraken->url($this->param);
        }

        if($response['success']){
            return file_get_contents($response['kraked_url']);
        }

        return false;
    }

    private function imageCrop(ImageFilterInterface $imageFilter)
    {
        if ($imageFilter instanceof ImageCropFilter) {
            $this->param['' . self::PARAM_RESIZE . '']['' . self::PARAM_STRATEGY . ''] = 'exact';
        }
    }

    private function imageFit(ImageFilterInterface $imageFilter)
    {
        if ($imageFilter->getBackground()) {
            $this->param[self::PARAM_RESIZE][self::PARAM_STRATEGY] = 'fill';
            $this->param[self::PARAM_RESIZE]['background'] = $imageFilter->getBackground();
        } else {
            $this->param[self::PARAM_RESIZE][self::PARAM_STRATEGY] = 'fit';
        }

    }

    public function setQuality(int $quality)
    {
        if ($quality < 0 || $quality > 100) {
            throw new InvalidArgumentException(sprintf("Quality: %s is invalid",$quality));
        }
        $this->param['quality'] = $quality;

    }

    /**
     * @param bool $mode
     */
    private function setFileMode(bool $mode)
    {
        $this->isFileMode = $mode;
        if ($mode) {
            unset($this->param['url']);
        } else {
            unset($this->param['file']);
        }
    }
}
