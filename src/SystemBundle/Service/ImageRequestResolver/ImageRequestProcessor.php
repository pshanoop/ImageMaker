<?php


namespace App\SystemBundle\Service\ImageRequestResolver;


use App\SystemBundle\Service\ImageRequestResolver\Utils\ColorNameToRgb;
use Symfony\Component\HttpFoundation\Request;
use App\SystemBundle\Service\ImageRequestResolver\Utils\UrlProcessor;
use App\SystemBundle\Service\ImageRequestResolver\DTO\ImageRequest;
use Symfony\Component\HttpFoundation\RequestStack;

class ImageRequestProcessor
{

    const REQUEST_REGEX = '/([a-zA-Z0-9\-_]+)-w([0-9]+)-h([0-9]+)(-q)?([0-9]+)?(-([a-z]+))?(\.(jpg|png))/';
    /** RegEx groups number for each attributes */
    const IMAGE_NAME = 1;
    const IMAGE_WIDTH = 2;
    const IMAGE_HEIGHT = 3;
    const IMAGE_QUALITY = 5;
    const IMAGE_BACKGROUND = 7;
    const IMAGE_EXT = 9;


    /**
     * @var Request
     */
    private $requestContext;
    /**
     * @var UrlProcessor
     */
    private $urlProcessor;

    /**
     * ImageRequestProcessor constructor.
     * @param RequestStack $requestContext
     * @param UrlProcessor $urlProcessor
     */
    public function __construct(RequestStack $requestContext, UrlProcessor $urlProcessor)
    {
        $this->requestContext = $requestContext;
        $this->urlProcessor = $urlProcessor;
    }

    public function getImageRequest()
    {
        $param = $this->urlProcessor->process(self::REQUEST_REGEX);

        if($param){
            $imageRequestDto = (new ImageRequest())
                ->setWidth($param[self::IMAGE_WIDTH])
                ->setHeight($param[self::IMAGE_HEIGHT])
                ->setQuality($param[self::IMAGE_QUALITY])
                ->setName($param[self::IMAGE_NAME])
                ->setExtension($param[self::IMAGE_EXT])
            ;

            //Convert color name to RGA
            $imageRequestDto
                ->setBackground(
                    ColorNameToRgb::getRGB($param[self::IMAGE_BACKGROUND])
                );

            return $imageRequestDto;
        }
        return null;
    }

    public static function getAttributes()
    {
        return [
            self::IMAGE_NAME,
            self::IMAGE_WIDTH,
            self::IMAGE_HEIGHT,
            self::IMAGE_QUALITY,
            self::IMAGE_BACKGROUND,
            self::IMAGE_EXT,
        ];
    }
}