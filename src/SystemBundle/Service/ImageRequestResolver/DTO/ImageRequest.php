<?php


namespace App\SystemBundle\Service\ImageRequestResolver\DTO;


class ImageRequest
{

    private $name;

    /** @var  Integer */
    private $width;
    /** @var  Integer */
    private $height;

    private $background = false;
    private $quality = false;

    private $extension;




    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     * @return ImageRequest
     */
    public function setWidth(int $width) : ImageRequest
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @param int $height
     * @return ImageRequest
     */
    public function setHeight(int $height): ImageRequest
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @param mixed $background
     * @return ImageRequest
     */
    public function setBackground($background)
    {
        $this->background = $background;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * @param int $quality
     * @return ImageRequest
     */
    public function setQuality($quality): ImageRequest
    {
        if (strlen($quality)) {
            $this->quality = $quality;
        }
        return $this;
    }

    /**
     * @return int
     */
    public function getQuality(): int
    {
        return $this->quality;
    }

    /**
     * @param mixed $name
     * @return ImageRequest
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $extension
     * @return ImageRequest
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }
}