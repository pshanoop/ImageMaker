<?php


namespace App\SystemBundle\Service\ImageRequestResolver\Utils;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class UrlProcessor
{

    /** @var  Request */
    private $request;
    /**
     * @var array
     */
    private $config;

    /**
     * UrlProcessor constructor.
     * @param array $config
     * @param RequestStack $request
     */
    public function __construct(array $config, RequestStack $request)
    {
        $this->request = $request->getCurrentRequest();
        $this->config = $config;
    }

    /**
     * @param string $matchRegEx
     * @return array|bool
     */
    public function process(string $matchRegEx)
    {
        $uri = $this->request->getRequestUri();
        $params = [];

        if(preg_match($matchRegEx,$uri,$params)){
            return $params;
        }
        else{
            return false;
        }


    }
}
