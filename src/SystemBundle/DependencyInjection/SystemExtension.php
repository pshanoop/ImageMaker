<?php


namespace App\SystemBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class SystemExtension extends Extension
{

    /**
     * Loads a specific configuration.
     *
     * @param array $configs An array of configuration values
     * @param ContainerBuilder $container A ContainerBuilder instance
     *
     * @throws \InvalidArgumentException When provided tag is not defined in this extension
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));

        $loader->load('services.yml');

        if ($container->hasDefinition('image.processor')) {
            $definition = $container->getDefinition('image.processor');
            $definition->replaceArgument(0,$config['image_processor']);
        }

        if ($container->hasDefinition('image.generator')) {
            $definition = $container->getDefinition('image.generator');
            $definition->replaceArgument(0, $config['image_generator']);
        }

        if ($container->hasDefinition('image.request.url.processor')) {
            $definition = $container->getDefinition('image.request.url.processor');
            $definition->replaceArgument(0, $config);
        }

    }
}