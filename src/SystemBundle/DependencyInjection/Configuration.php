<?php


namespace App\SystemBundle\DependencyInjection;


use App\SystemBundle\Service\Image\Factory\ImageProcessorFactory;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $rootNode  = $treeBuilder->root('system');

        $rootNode
            ->children()
                ->arrayNode('image_generator')
                    ->children()
                        ->scalarNode('directory')
                            ->isRequired()
                            ->cannotBeEmpty()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('image_processor')
                    ->children()
                        ->scalarNode('driver')
                            ->isRequired()
                            ->validate()
                                ->ifNotInArray(ImageProcessorFactory::getAvailableDrivers())
                                ->thenInvalid("%s driver is not found")
                            ->end()
                        ->end()
                        ->scalarNode('kraken_key')->isRequired()->end()
                        ->scalarNode('kraken_secret')->isRequired()->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}