<?php

namespace Tests\App\SystemBundle\Service\Image\Factory;

use App\SystemBundle\Service\Image\Drivers\KrakenIo;
use App\SystemBundle\Service\Image\Factory\ImageProcessorFactory;
use PHPUnit\Framework\TestCase;

class ImageProcessorFactoryTest extends TestCase
{

    public function testCreategstKraken()
    {
        $config = [
            'driver' => 'kraken',
            'kraken_key' => 'something',
            'kraken_secret' => 'something secret',
            'is_lossy' => 'true',
        ];

        $imageProcessor = ImageProcessorFactory::create($config);
        $this->assertInstanceOf(KrakenIo::class, $imageProcessor);
    }
}
