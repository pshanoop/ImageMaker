<?php

namespace Tests\App\SystemBundle\Service\ImageRequestResolver;

use App\SystemBundle\Service\ImageRequestResolver\DTO\ImageRequest;
use App\SystemBundle\Service\ImageRequestResolver\ImageRequestProcessor;
use App\SystemBundle\Service\ImageRequestResolver\Utils\ColorNameToRgb;
use App\SystemBundle\Service\ImageRequestResolver\Utils\UrlProcessor;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Symfony\Component\HttpFoundation\RequestStack;

class ImageRequestProcessorTest extends TestCase
{

    /** @var  PHPUnit_Framework_MockObject_MockObject */
    private $requestStackContext;
    /** @var  PHPUnit_Framework_MockObject_MockObject */
    private $urlProcessor;

    public function initMock()
    {

        $this->requestStackContext = $this->getMockBuilder(RequestStack::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->urlProcessor = $this->getMockBuilder( UrlProcessor::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @param $a
     * @param $expected
     * @dataProvider  paramDataProvider()
     */
    public function testGetImageRequest($a,$expected)
    {
        $this->initMock();

        $this->urlProcessor
            ->method('process')
            ->willReturn($a)
        ;

        $imageRequestProcessor = new ImageRequestProcessor($this->requestStackContext, $this->urlProcessor);

        $result = $imageRequestProcessor->getImageRequest();

        $this->assertEquals($expected,$result);
    }

    /**
     * @return array
     */
    public function paramDataProvider() : array
    {

        $param = $this->getParamArray(['test', '200', '100', '100', 'white', 'png',]);
        $imageRequest = new ImageRequest();
        $imageRequest
            ->setWidth($param[ImageRequestProcessor::IMAGE_WIDTH])
            ->setHeight($param[ImageRequestProcessor::IMAGE_HEIGHT])
            ->setQuality($param[ImageRequestProcessor::IMAGE_QUALITY])
            ->setName($param[ImageRequestProcessor::IMAGE_NAME])
            ->setExtension($param[ImageRequestProcessor::IMAGE_EXT])
        ;

        $imageRequest
            ->setBackground(
                ColorNameToRgb::getRGB($param[ImageRequestProcessor::IMAGE_BACKGROUND])
            );

        return [
            [$param,$imageRequest],
            [false, null]
        ];

    }

    private function getParamArray(array $param) : array
    {
        $attributes = ImageRequestProcessor::getAttributes();

        $params = [];

        foreach ($attributes as $key => $attribute ){
                $params[$attribute] = $param[$key];
        }

        return $params;
    }
}
