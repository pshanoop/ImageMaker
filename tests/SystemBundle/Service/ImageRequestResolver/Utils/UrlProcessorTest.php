<?php

namespace Tests\App\SystemBundle\Service\ImageRequestResolver\Utils;

use App\SystemBundle\Service\ImageRequestResolver\ImageRequestProcessor;
use App\SystemBundle\Service\ImageRequestResolver\Utils\UrlProcessor;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class UrlProcessorTest extends TestCase
{
    /** @var \PHPUnit_Framework_MockObject_MockObject */
    private $requestStackMock;

    /** @var \PHPUnit_Framework_MockObject_MockObject */
    private  $requestMock;

    public function initMock()
    {
        $this->requestStackMock = $this->getMockBuilder(RequestStack::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->requestMock = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->requestStackMock->method('getCurrentRequest')->willReturn($this->requestMock);


    }

    /**
     * @param $url
     * @param $expected
     * @dataProvider urlProvider()
     */
    public function testProcess($url,$expected)
    {
        $this->initMock();

        $this->requestMock
            ->method('getRequestUri')
            ->willReturn($url);

        $urlProcessor = new UrlProcessor([], $this->requestStackMock);
        $params = $urlProcessor->process(ImageRequestProcessor::REQUEST_REGEX);

        $this->assertEquals($expected,$params);
    }

    public function urlProvider()
    {
        return [
            [
                'https://img.example.com/v5/8888-8555-karmelet-w200-h500-transparent.jpg',
                ['8888-8555-karmelet-w200-h500-transparent.jpg','8888-8555-karmelet','200','500','','','-transparent','transparent','.jpg','jpg']
            ],
            [
                'https://img.example.com/v5/8888-8555-karmelet-w200-h500-white.png',
                ['8888-8555-karmelet-w200-h500-white.png','8888-8555-karmelet','200','500','','','-white','white','.png','png']
            ],
            [
                'https://img.example.com/v5/8888-8555-karmelet-w200-h500-q100-white.png',
                ['8888-8555-karmelet-w200-h500-q100-white.png','8888-8555-karmelet','200','500','-q','100','-white','white','.png','png']
            ],
            [
                'https://img.example.com/v5/8888-8555-karmeletfsafsdfsa-ds.png',
                false
            ]
        ];
    }

}
